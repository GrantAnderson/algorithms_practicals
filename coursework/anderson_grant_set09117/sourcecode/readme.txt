To start a game, run checkers.py

Choose number of players and, if applicable, difficulty of computer player(s).

Choices will be displayed in a list under the game board.
Choices show a number, start cell and end cell.

Enter your choice as an integer from the list.