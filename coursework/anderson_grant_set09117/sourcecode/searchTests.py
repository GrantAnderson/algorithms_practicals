import time

def linearSearch(data,key):
    for i, v in enumerate(data):
        if v == key:
            return i
    return -1

def binarySearch(data,key):
    first = 0
    last = len(data)-1
    found = False

    while first <= last and not found:
        midpoint = (first + last)//2
        if midpoint == key:
            found = True
            return midpoint
        else:
            if key < midpoint:
                last = midpoint-1
            else:
                first = midpoint+1
    return -1

# Starting positions
def initialise_positions(p1_pos, p2_pos):
    # Starting positions for player 1
    for cell in ["a7", "a5", "b6", "c7", "c5", "d6", "e7", "e5", "f6", "g7", "g5", "h6"]:
        p1_pos[cell] = " X "

    # Starting positions for player 2
    for cell in ["a1", "b0", "b2", "c1", "d0", "d2", "e1", "f0", "f2", "g1", "h0", "h2"]:
        p2_pos[cell] = " O "

# Starting positions
def initialise_positions_numbers(p1_pos, p2_pos):
    # Starting positions for player 1
    for cell in [7, 5, 16, 27, 25, 36, 47, 45, 56, 67, 65, 76]:
        p1_pos[cell] = " X "

    # Starting positions for player 2
    for cell in [1, 10, 12, 21, 30, 32, 41, 50, 52, 61, 70, 72]:
        p2_pos[cell] = " O "

def main():
    p1_pos = dict()
    p2_pos = dict()

    initialise_positions(p1_pos, p2_pos)
    p1_pos = sorted(p1_pos)
    p2_pos = sorted(p2_pos)
    cells = ["a0", "a1", "a2", "a3","a4","a5","a6","a7",
             "b0", "b1", "b2", "b3","b4","b5","b6","b7",
             "c0", "c1", "c2", "c3","c4","c5","c6","c7",
             "d0", "d1", "d2", "d3","d4","d5","d6","d7",
             "e0", "e1", "e2", "e3","e4","e5","e6","e7",
             "f0", "f1", "f2", "f3","f4","f5","f6","f7",
             "g0", "g1", "g2", "g3","g4","g5","g6","g7",
             "h0", "h1", "h2", "h3","h4","h5","h6","h7"]
    total_time = 0.0
    number = 0
    for a in range(0, 5000):
        for i in cells:
            start = time.time()
            result = linearSearch(p1_pos, i)
            result_2 = linearSearch(p2_pos, i)
            time_taken = time.time() - start
            total_time = total_time + time_taken
            number = number + 64
    average = total_time / number
    print("Linear search: ", average)

    p1_pos = dict()
    p2_pos = dict()

    initialise_positions_numbers(p1_pos, p2_pos)
    p1_pos = sorted(p1_pos)
    p2_pos = sorted(p2_pos)
    cells = [0, 1, 2, 3,4,5,6,7,
             10, 11, 12, 13,14,15,16,17,
             20, 21, 22, 23,24,25,26,27,
             30, 31, 32, 33,34,35,36,37,
             40, 41, 42, 43,44,45,46,47,
             50, 51, 52, 53,54,55,56,57,
             60, 61, 62, 63,64,65,66,67,
             70, 71, 72, 73,74,75,76,77]
    total_time = 0.0
    number = 0
    for a in range(0, 5000):
        for i in cells:
            start = time.time()
            result = binarySearch(p1_pos, i)
            result_2 = binarySearch(p2_pos, i)
            time_taken = time.time() - start
            total_time = total_time + time_taken
            number = number + 64
    average = total_time / number
    print("Binary search: ", average)
    
    p1_pos = dict()
    p2_pos = dict()

    initialise_positions(p1_pos, p2_pos)
    p1_pos = sorted(p1_pos)
    p2_pos = sorted(p2_pos)
    cells = ["a0", "a1", "a2", "a3","a4","a5","a6","a7",
             "b0", "b1", "b2", "b3","b4","b5","b6","b7",
             "c0", "c1", "c2", "c3","c4","c5","c6","c7",
             "d0", "d1", "d2", "d3","d4","d5","d6","d7",
             "e0", "e1", "e2", "e3","e4","e5","e6","e7",
             "f0", "f1", "f2", "f3","f4","f5","f6","f7",
             "g0", "g1", "g2", "g3","g4","g5","g6","g7",
             "h0", "h1", "h2", "h3","h4","h5","h6","h7"]
    total_time = 0.0
    number = 0
    for a in range(0, 5000):
        for i in cells:
            start = time.time()
            result = i in p1_pos
            result_2 = i in p2_pos
            time_taken = time.time() - start
            total_time = total_time + time_taken
            number = number + 64
    average = total_time / number
    print("In search: ", average)


if __name__ == "__main__":
    main()
