# Import game functions
from game import *
# Import deques
from collections import deque

def main():
    # Set up data structures needed
    # Player 1 and 2's piece positions
    p1_pos = dict()
    p2_pos = dict()
    # List of legal moves
    legal_moves = list()
    # Move history
    move_list = deque()
    redo_list = deque()
    
    # Set up the board with the starting positions
    game = Game()
    game.initialise_positions(p1_pos, p2_pos)
    player = "1"

    # Get number of players
    players = str()
    while players not in {"0", "1", "2"}:
        players = input("Enter number of players (0, 1 or 2): ")
        if players not in {"0", "1", "2"}:
            print("Error")

    # Get AI difficulty if applicable
    if players == "1" or players == "0":
        difficulty2 = int()
        while difficulty2 not in range(4, 11):
            difficulty2 = input("Enter Player 2 AI difficulty (4 - 10): ")
            try:
               difficulty2 = int(difficulty2)
            except ValueError:
               print("Difficulty needs to be integer between 4 and 10")
            if difficulty2 not in range(4, 11):
                print("Error")
        if players == "0":
            difficulty1 = int()
            while difficulty1 not in range(4, 11):
                difficulty1 = input("Enter Player 1 AI difficulty (4 - 10): ")
                try:
                   difficulty1 = int(difficulty1)
                except ValueError:
                   print("Difficulty needs to be integer between 4 and 10")
                if difficulty1 not in range(4, 11):
                    print("Error")
        

    # Begin game
    while True:
        # Reset for new turn
        jump_flag = False
        legal_moves.clear()
        # Display the board
        game.print_board(p1_pos, p2_pos)
        # Player 1's turn
        if player == "1":
            # Create a state to get to get legal moves
            state = Node(1, p1_pos, p2_pos)
            # AI player 1
            if players == "0":
                player = game.ai_turn(game, player, state, p1_pos, p2_pos, difficulty1, legal_moves, move_list)
            # Human player 1
            else:
                player = game.player_turn(player, state, p1_pos, p2_pos, legal_moves, move_list, redo_list)
        # Player 2's turn
        else:
            # Create a state to get to get legal moves
            state = Node(2, p1_pos, p2_pos)
            # Human player 2
            if players == "2":
                player = game.player_turn(player, state, p1_pos, p2_pos, legal_moves, move_list, redo_list)
            # AI player 2
            else:                
                player = game.ai_turn(game, player, state, p1_pos, p2_pos, difficulty2, legal_moves, move_list)
            
        if player == 0:
            break


    # Display option to replay game            
    replay = input("Replay? y/n")
    if replay == "y":
        # Reset the game and display the board
        p1_pos.clear()
        p2_pos.clear()
        player = "1"
        game.initialise_positions(p1_pos, p2_pos)
        game.print_board(p1_pos, p2_pos)

        # Wait for user to step through
        input()

        # For each move...
        for move in move_list:
            # Print the details of the move
            game.print_move(move)
            # Perform the move
            game.update_positions(str(move.player), p1_pos, p2_pos, move)
            # King the piece if necessary
            if str(move.player) == "1":
                game.king_piece(str(move.player), p1_pos, move)
            else:
                game.king_piece(str(move.player), p2_pos, move)
            # Display the board
            game.print_board(p1_pos, p2_pos)
            input()

                

if __name__ == "__main__":
    main()
