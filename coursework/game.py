# Import AI
from ai import *

# Node class: (stores info about a game state)
# - whose turn
# - positions of player 1
# - positions of player 2
class Node:
    def __init__(self, player, p1_pos, p2_pos):
        self.player = player
        self.p1_pos = p1_pos
        self.p2_pos = p2_pos

# Move class:
# - a start cell
# - an end cell
# - cell of taken piece if the move took one
# - piece type of taken piece
# - player that made the move
# - whether or not the current piece was kinged on this move
class Move:
    def __init__(self, start, end, take_cell, take_piece, player, kinged):
        self.start = start
        self.end = end
        self.take_cell = take_cell
        self.take_piece = take_piece
        self.player = player
        self.kinged = kinged

# Game class:
# Contains functions to play a game of checkers
class Game:
    # Starting positions
    def initialise_positions(self, p1_pos, p2_pos):
        # Starting positions for player 1
        for cell in ["a7", "a5", "b6", "c7", "c5", "d6", "e7", "e5", "f6", "g7", "g5", "h6"]:
            p1_pos[cell] = " X "

        # Starting positions for player 2
        for cell in ["a1", "b0", "b2", "c1", "d0", "d2", "e1", "f0", "f2", "g1", "h0", "h2"]:
            p2_pos[cell] = " O "

    # Successors gets all the possible moves from the current state
    def successors(self, state):
        moves = list()
        # Check for jump moves first and if there
        # are none available, check for standard moves
        if state.player == 1:
            self.check_jump(state.player, state.p1_pos, state.p2_pos, moves)
            if len(moves) == 0:
                self.get_legal_moves(state.player, state.p1_pos, state.p2_pos, moves)
        else:
            self.check_jump(state.player, state.p2_pos, state.p1_pos, moves)
            if len(moves) == 0:
                self.get_legal_moves(state.player, state.p2_pos, state.p1_pos, moves)
        # Check if the move should result in the piece being 'kinged'
        for move in moves:
            if move.player == 1 and move.end[1] == "0" and state.p1_pos[move.start] != "KX ":
                move.kinged = "yes"
            if move.player == 2 and move.end[1] == "7" and state.p2_pos[move.start] != "KO ":
                move.kinged = "yes"
        # Return the list of legal moves
        return moves

    # Get non-jumping legal moves for the current player
    def get_legal_moves(self, player, current_player_pos, opponent_pos, legal_moves):
        # For each of the player's pieces...
        for cell in current_player_pos:
            col = cell[0]
            row = int(cell[1])
            # Player 1's standard moves or player 2's Kinged moves
            if player == 1 or current_player_pos[cell] == "KO ":
                # Moves to the right and up
                if col != "h" and row != 0:
                    # Check the square up and to the right
                    check1 = chr(ord(col) + 1) + str(row - 1)
                    own_piece_present = check1 in current_player_pos
                    opp_piece_present = check1 in opponent_pos
                    # If square is free, add the move
                    if own_piece_present == False and opp_piece_present == False:
                        possible = Move(cell, check1, " ", " ", player, " ")
                        legal_moves.append(possible)
                        
                # Moves to the left and up  
                if col != "a" and row != 0:
                    # Check the square up and to the left
                    check1 = chr(ord(col) - 1) + str(row - 1)
                    own_piece_present = check1 in current_player_pos
                    opp_piece_present = check1 in opponent_pos
                    # If square is free, add the move
                    if own_piece_present == False and opp_piece_present == False:
                        possible = Move(cell, check1, " ", " ", player, " ")
                        legal_moves.append(possible)
                        
            # Player 2's standard moves or player 1's Kinged moves                
            if player == 2 or current_player_pos[cell] == "KX ":
                # Moves to the right and down
                if col != "h" and row != 7:
                    # Check the square down and to the right
                    check1 = chr(ord(col) + 1) + str(row + 1)
                    own_piece_present = check1 in current_player_pos
                    opp_piece_present = check1 in opponent_pos
                    # If square is free, add the move
                    if own_piece_present == False and opp_piece_present == False:
                        possible = Move(cell, check1, " ", " ", player, " ")
                        legal_moves.append(possible)
                        
                # Moves to the left and down  
                if col != "a" and row != 7:
                    # Check the square down and to the left
                    check1 = chr(ord(col) - 1) + str(row + 1)
                    own_piece_present = check1 in current_player_pos
                    opp_piece_present = check1 in opponent_pos
                    # If square is free, add the move
                    if own_piece_present == False and opp_piece_present == False:
                        possible = Move(cell, check1, " ", " ", player, " ")
                        legal_moves.append(possible)

    # Check for mandatory jump moves
    def check_jump(self, player, current_player_pos, opponent_pos, legal_moves):
        # For each of the player's pieces...
        for cell in current_player_pos:
            col = cell[0]
            row = int(cell[1])
            # Check for opponent piece and empty square beyond
            # Player 1 jumps or Player 2 King jumps
            if player == 1 or current_player_pos[cell] == "KO ":
                # Right and up
                if col <= "f" and row >= 2:
                    # Check if there is an opponent piece to be taken
                    check_opp = chr(ord(col) + 1) + str(row - 1)
                    opp_present = check_opp in opponent_pos
                    if opp_present == True:
                        # Check if there is an empty square beyond
                        check_empty = chr(ord(col) + 2) + str(row - 2)
                        own_present = check_empty in current_player_pos
                        opp_present = check_empty in opponent_pos
                        if own_present == False and opp_present == False:
                            # If all tests pass, add the move
                            jump_move = Move(cell, check_empty, check_opp, opponent_pos[check_opp], player, " ")
                            legal_moves.append(jump_move)

                # Left and up
                if col >= "c" and row >= 2:
                    # Check if there is an opponent piece to be taken
                    check_opp = chr(ord(col) - 1) + str(row - 1)
                    opp_present = check_opp in opponent_pos
                    if opp_present == True:
                        # Check if there is an empty square beyond
                        check_empty = chr(ord(col) - 2) + str(row - 2)
                        own_present = check_empty in current_player_pos
                        opp_present = check_empty in opponent_pos
                        if own_present == False and opp_present == False:
                            # If all tests pass, add the move
                            jump_move = Move(cell, check_empty, check_opp, opponent_pos[check_opp], player, " ")
                            legal_moves.append(jump_move)
                            
            # Player 2 jumps or Player 1 King jumps
            if player == 2 or current_player_pos[cell] == "KX ":
                # Right and down
                if col <= "f" and row <= 5:
                    # Check if there is an opponent piece to be taken
                    check_opp = chr(ord(col) + 1) + str(row + 1)
                    opp_present = check_opp in opponent_pos
                    if opp_present == True:
                        # Check if there is an empty square beyond
                        check_empty = chr(ord(col) + 2) + str(row + 2)
                        own_present = check_empty in current_player_pos
                        opp_present = check_empty in opponent_pos
                        if own_present == False and opp_present == False:
                            # If all tests pass, add the move
                            jump_move = Move(cell, check_empty, check_opp, opponent_pos[check_opp], player, " ")
                            legal_moves.append(jump_move)

                # Left and down
                if col >= "c" and row <= 5:
                    # Check if there is an opponent piece to be taken
                    check_opp = chr(ord(col) - 1) + str(row + 1)
                    opp_present = check_opp in opponent_pos
                    if opp_present == True:
                        # Check if there is an empty square beyond
                        check_empty = chr(ord(col) - 2) + str(row + 2)
                        own_present = check_empty in current_player_pos
                        opp_present = check_empty in opponent_pos
                        if own_present == False and opp_present == False:
                            # If all tests pass, add the move
                            jump_move = Move(cell, check_empty, check_opp, opponent_pos[check_opp], player, " ")
                            legal_moves.append(jump_move)

    # Returns the state reached by performing the move (used by AI)
    def perform_move(self, state, move):
        new_p1_pos = dict(state.p1_pos)
        new_p2_pos = dict(state.p2_pos)

        # Move the piece from the start cell to the end cell
        # Check if the move took an opponent piece and remove
        # it if so
        # Check if the piece is to be 'kinged'
        # Create and return the new state
        if state.player == 1:
            piece = new_p1_pos[move.start]
            del(new_p1_pos[move.start])
            new_p1_pos[move.end] = piece
            if move.take_cell != " ":
                del(new_p2_pos[move.take_cell])
            if move.kinged != " ":
                new_p1_pos[move.end] = "KX "
            new_state = Node(2, new_p1_pos, new_p2_pos)
            return new_state
        if state.player == 2:
            piece = new_p2_pos[move.start]
            del(new_p2_pos[move.start])
            new_p2_pos[move.end] = piece
            if move.take_cell != " ":
                del(new_p1_pos[move.take_cell])
            if move.kinged != " ":
                new_p2_pos[move.end] = "KO "
            new_state = Node(1, new_p1_pos, new_p2_pos)
            return new_state

    # Update each player's positions as the result of a move    
    def update_positions(self, player, p1_pos, p2_pos, move):
        # Move the piece from the start cell to the end cell
        # Check if the move took an opponent piece and remove
        # it if so (also return True to set the jump flag)
        if player == "1":
            piece = p1_pos[move.start]
            del(p1_pos[move.start])
            p1_pos[move.end] = piece
            if move.take_cell != " ":
                del(p2_pos[move.take_cell])
                return True
        else:
            piece = p2_pos[move.start]
            del(p2_pos[move.start])
            p2_pos[move.end] = piece
            if move.take_cell != " ":
                del(p1_pos[move.take_cell])
                return True

    # Check if a piece is to be 'kinged' and update it if so
    def king_piece(self, player, pos, move):
        # If a piece has reached the end of the board and is
        # not already a 'king', king it
        row = int(move.end[1])
        if player == "1":
            if row == 0 and pos[move.end] != "KX ":
                pos[move.end] = "KX "
        else:
            if row == 7 and pos[move.end] != "KO ":
                pos[move.end] = "KO "
            
    # Undo the last two moves made
    def undo(self, p1_pos, p2_pos, move_list, redo_list):
        # Undo other player's last move
        last_move = move_list.pop()
        # Update player positions
        if last_move.player == 2:
            piece = p2_pos[last_move.end]
            del(p2_pos[last_move.end])
            p2_pos[last_move.start] = piece
            if last_move.take_cell != " ":
                p1_pos[last_move.take_cell] = last_move.take_piece
            # Push this move onto the redo_list
            redo_list.append(last_move)
            
            # Undo this player's last move
            last_move = move_list.pop()
            # Update player positions
            piece = p1_pos[last_move.end]
            del(p1_pos[last_move.end])
            p1_pos[last_move.start] = piece
            if last_move.take_cell != " ":
                p2_pos[last_move.take_cell] = last_move.take_piece
            # Push this move onto the redo_list
            redo_list.append(last_move)
        else:
            piece = p1_pos[last_move.end]
            del(p1_pos[last_move.end])
            p1_pos[last_move.start] = piece
            if last_move.take_cell != " ":
                p2_pos[last_move.take_cell] = last_move.take_piece
            # Push this move onto the redo_list
            redo_list.append(last_move)
            
            # Undo this player's last move
            last_move = move_list.pop()
            # Update player positions
            piece = p2_pos[last_move.end]
            del(p2_pos[last_move.end])
            p2_pos[last_move.start] = piece
            if last_move.take_cell != " ":
                p1_pos[last_move.take_cell] = last_move.take_piece
            # Push this move onto the redo_list
            redo_list.append(last_move)

    # Redo the last two moves that have been undone
    def redo(self, p1_pos, p2_pos, move_list, redo_list):
        # Redo this player's last move
        re_move = redo_list.pop()
        # Update player positions
        if re_move.player == 2:
            piece = p2_pos[re_move.start]
            del(p2_pos[re_move.start])
            p2_pos[re_move.end] = piece
            if re_move.take_cell != " ":
                del(p1_pos[re_move.take_cell])
            # Push this move onto the move_list
            move_list.append(re_move)

            # Redo other player's last move
            re_move = redo_list.pop()
            # Update player positions
            piece = p1_pos[re_move.start]
            del(p1_pos[re_move.start])
            p1_pos[re_move.end] = piece
            if re_move.take_cell != " ":
                del(p2_pos[re_move.take_cell])
            # Push this move onto the move_list
            move_list.append(re_move)
        else:
            piece = p1_pos[re_move.start]
            del(p1_pos[re_move.start])
            p1_pos[re_move.end] = piece
            if re_move.take_cell != " ":
                del(p2_pos[re_move.take_cell])
            # Push this move onto the move_list
            move_list.append(re_move)

            # Redo other player's last move
            re_move = redo_list.pop()
            # Update player positions
            piece = p2_pos[re_move.start]
            del(p2_pos[re_move.start])
            p2_pos[re_move.end] = piece
            if re_move.take_cell != " ":
                del(p1_pos[re_move.take_cell])
            # Push this move onto the move_list
            move_list.append(re_move)
    
    # Score a game state
    def utility(self, state):
        # At the start of the game:
        # - Each enemy 'king' is worth -5 points
        # - Each enemy 'man' is worth -1 point
        # - Each own 'king' is worth +5 points
        # - Each own 'man' is worht +1 point

        # When the opponent has less than 5 pieces left:
        # - Each enemy 'king' is worth -5 points
        # - Each enemy 'man' is worth -1 point
        # - Own pieces are ignored
        score = 0
        if state.player == 1:
            if len(state.p2_pos) < 5:
                for cell in state.p2_pos:
                    if state.p2_pos[cell] == "KO":
                        score -= 5
                    else:
                        score -= 1
            else:
                for cell in state.p1_pos:
                    if state.p1_pos[cell] == "KX ":
                        score += 5
                    else:
                        score += 1
                for cell in state.p2_pos:
                    if state.p2_pos[cell] == "KO":
                        score -= 5
                    else:
                        score -= 1
            return score
        if state.player == 2:
            if len(state.p1_pos) < 5:
                for cell in state.p1_pos:
                    if state.p1_pos[cell] == "KX":
                        score -= 5
                    else:
                        score -= 1
            else:
                for cell in state.p2_pos:
                    if state.p2_pos[cell] == "KO ":
                        score += 5
                    else:
                        score += 1
                for cell in state.p1_pos:
                    if state.p1_pos[cell] == "KX":
                        score -= 5
                    else:
                        score -= 1
            return score
        
    # Check if this state is an end state
    def terminal_test(self, state):
        return not self.successors(state)

    # Prints the game board with the pieces
    def print_board(self, p1_pos, p2_pos):
        # Display column letters for grid
        print("   a    b    c    d    e    f    g    h")
        print("   --------------------------------------", end='')
        # For each row...
        for i in range(8):
            # Print the row number
            print("\n", i, end = '')
            # For each column...
            for a in "abcdefgh":
                # Make a key for this cell
                cell = a + str(i)
                # If it isn't in the last column print the separator
                if a != "h":
                    # If there's a piece in this cell, print it
                    if cell in p1_pos:
                        print(p1_pos[cell], "|", end = '')
                    elif cell in p2_pos:
                        print(p2_pos[cell], "|", end = '')
                    else:
                        print("   ", "|", end = '')
                # If it is the last column don't print the separator        
                else:
                    # If there's a piece in this cell, print it
                    if cell in p1_pos:
                        print(p1_pos[cell], end = '')
                    elif cell in p2_pos:
                        print(p2_pos[cell], end = '')
                    else:
                        print("   ", end = '')
            print("\n", "  --------------------------------------", end = '')
        # Display column letters for grid
        print("\n", "   a    b    c    d    e    f    g    h")

    # Print player's legal moves
    def print_legal_moves(self, legal_moves):
        print("Legal moves")
        i = 0
        for move in legal_moves:
            print(i, ") ", move.start, " to ", move.end)
            i = i + 1

    # Print the details of a move
    def print_move(self, move):
        print("Player ", move.player, " moves from ", move.start, " to ", move.end)
        if move.take_cell != " ":
            print(" taking opponent's ", move.take_piece, " at ", move.take_cell)
        if move.kinged == "yes":
            print("and gets upgraded to a King")

    # Display choices to player and take input
    def get_choice(self, player, move_list, redo_list, legal_moves):
        print("-1 to quit")
        if len(move_list) > 1:
            print("-2 to undo")
        if len(redo_list) > 1:
            print("-3 to redo")
        self.print_legal_moves(legal_moves)
        if player == "1":
            choice_chr = input("P1 enter your move: ")
        else:
            choice_chr = input("P2 enter your move: ")
        return choice_chr

    # Perform all necessary steps to complete a human player's turn
    def player_turn(self, player, state, p1_pos, p2_pos, legal_moves, move_list, redo_list):
        # Get player 1's legal moves
        legal_moves = self.successors(state)
                
        # Check for endgame
        if len(legal_moves) == 0:
            if player == "1":
                print("Player 2 wins")
            else:
                print("Player 1 wins")
            return 0
                
                
        # Get choice
        choice = str()
        while type(choice) is not int:
            # Display choices
            choice_chr = self.get_choice(player, move_list, redo_list, legal_moves)
            try:
               choice = int(choice_chr)
            except ValueError:
               print("Difficulty needs to be integer choice from list")
        if choice == -1:
            if player == "1":
                print("Player 1 forfeits the game")
                print("Player 2 wins")
            else:
                print("Player 2 forfeits the game")
                print("Player 1 wins")
            return 0 
        # Undo function
        elif choice == -2:
            if len(move_list) < 2:
                print("No move to undo")
            else:
                self.undo(p1_pos, p2_pos, move_list, redo_list)
            if player == "1":
                return "1"
            else:
                return "2"
        # Redo function
        elif choice == -3:
            if len(redo_list) < 2:
                print("No move to redo")
            else:
                self.redo(p1_pos, p2_pos, move_list, redo_list)
            if player == "1":
                return "1"
            else:
                return "2"
        elif choice >= len(legal_moves) or choice < -3:
            print("Error")
            if player == "1":
                return "1"
            else:
                return "2"
        redo_list.clear()
        move = legal_moves[choice]
                
        # Update positions and check if a jump occurred
        jump_flag = self.update_positions(player, p1_pos, p2_pos, move)
        # Check if piece has reached the end of the board and isn't a King already
        if player == "1":
            self.king_piece(player, p1_pos, move)
        else:
            self.king_piece(player, p2_pos, move)
        # Add to move list and pass play
        move_list.append(move)
        if player == "1":
            player = "2"
        else:
            player = "1"
            
        # Print the details of the move
        self.print_move(move)
                
        # If a jump occurred, check for chain possibility
        if jump_flag:
            active_piece = dict()
            if player == "1":
                active_piece[move.end] = p2_pos[move.end]
            else:
                active_piece[move.end] = p1_pos[move.end]
            legal_moves.clear()
            # Check for further jumps
            if player == "1":
                self.check_jump(2, active_piece, p1_pos, legal_moves)
            else:
                self.check_jump(1, active_piece, p2_pos, legal_moves)
            # If chain is possible, player gets another turn
            if len(legal_moves) != 0:
                if player == "1":
                    player = "2"
                else:
                    player = "1"
        return player

    # Perform all necessary steps to complete an AI player's turn
    def ai_turn(self, game, player, state, p1_pos, p2_pos, difficulty, legal_moves, move_list):
        # Search for the best move to make
        print("Player ", player, " is thinking...")
        move = alphabeta_cutoff_search(game, state, difficulty)

        # Check for endgame
        if move is None:
            if player == "1":
                print("Player 2 wins")
            else:
                print("Player 1 wins")
            return 0

        # Update positions and check if a jump occurred
        jump_flag = game.update_positions(player, p1_pos, p2_pos, move)

        # Check if piece has reached the end of the board and isn't a King already
        if player == "1":
            game.king_piece(player, p1_pos, move)
        else:
            game.king_piece(player, p2_pos, move)

        # Add to move list and pass play
        move_list.append(move)
        if player == "1":
            player = "2"
        else:
            player = "1"

        # Print the details of the move
        game.print_move(move)

        # If a jump occurred, check for chain possibility
        if jump_flag:
            active_piece = dict()
            if player == "1":
                active_piece[move.end] = p2_pos[move.end]
            else:
                active_piece[move.end] = p1_pos[move.end]
            legal_moves.clear()
            # Check for further jumps
            if player == "1":
                game.check_jump(2, active_piece, p1_pos, legal_moves)
            else:
                game.check_jump(1, active_piece, p2_pos, legal_moves)
            # If chain is possible, player gets another turn
            if len(legal_moves) != 0:
                if player == "1":
                    player = "2"
                else:
                    player = "1"
        return player
