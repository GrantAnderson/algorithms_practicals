%FILL THESE IN
\def\mytitle{Algorithms \& Data Structures Coursework Report}
\def\mykeywords{checkers, ai, alpha-beta, algorithms, data structures, python}
\def\myauthor{Grant Anderson}
\def\contact{40218994@napier.live.ac.uk}
\def\mymodule{Algorithms \& Data Structures (SET09117)}
%YOU DON'T NEED TO TOUCH ANYTHING BELOW
\documentclass[10pt, a4paper]{article}
\usepackage[a4paper,outer=1.5cm,inner=1.5cm,top=1.75cm,bottom=1.5cm]{geometry}
\twocolumn
\usepackage{graphicx}
\graphicspath{{./images/}}
%colour our links, remove weird boxes
\usepackage[colorlinks,linkcolor={black},citecolor={blue!80!black},urlcolor={blue!80!black}]{hyperref}
%Stop indentation on new paragraphs
\usepackage[parfill]{parskip}
%% all this is for Arial
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{uarial}
\renewcommand{\familydefault}{\sfdefault}
%Napier logo top right
\usepackage{watermark}
%Lorem Ipusm dolor please don't leave any in you final repot ;)
\usepackage{lipsum}
\usepackage{xcolor}
\usepackage{listings}
%give us the Capital H that we all know and love
\usepackage{float}
%tone down the linespacing after section titles
\usepackage{titlesec}
%Cool maths printing
\usepackage{amsmath}
%PseudoCode
\usepackage{algorithm2e}

\titlespacing{\subsection}{0pt}{\parskip}{-3pt}
\titlespacing{\subsubsection}{0pt}{\parskip}{-\parskip}
\titlespacing{\paragraph}{0pt}{\parskip}{\parskip}
\newcommand{\figuremacro}[5]{
    \begin{figure}[#1]
        \centering
        \includegraphics[width=#5\columnwidth]{#2}
        \caption[#3]{\textbf{#3}#4}
        \label{fig:#2}
    \end{figure}
}

\lstset{
	escapeinside={/*@}{@*/}, language=C++,
	basicstyle=\fontsize{8.5}{12}\selectfont,
	numbers=left,numbersep=2pt,xleftmargin=2pt,frame=tb,
    columns=fullflexible,showstringspaces=false,tabsize=4,
    keepspaces=true,showtabs=false,showspaces=false,
    backgroundcolor=\color{white}, morekeywords={inline,public,
    class,private,protected,struct},captionpos=t,lineskip=-0.4em,
	aboveskip=10pt, extendedchars=true, breaklines=true,
	prebreak = \raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
	keywordstyle=\color[rgb]{0,0,1},
	commentstyle=\color[rgb]{0.133,0.545,0.133},
	stringstyle=\color[rgb]{0.627,0.126,0.941}
}

\thiswatermark{\centering \put(336.5,-38.0){\includegraphics[scale=0.8]{logo}} }
\title{\mytitle}
\author{\myauthor\hspace{1em}\\\contact\\Edinburgh Napier University\hspace{0.5em}-\hspace{0.5em}\mymodule}
\date{}
\hypersetup{pdfauthor=\myauthor,pdftitle=\mytitle,pdfkeywords=\mykeywords}

\sloppy
\begin{document}
	\maketitle
	\textbf{Keywords -- }{\mykeywords}
   	 %START FROM HERE
\section{Introduction}
	The goal of this project was to develop a checkers game that showcases an understanding of the theory and practice of algorithms and data structures. The game was required to have several built in features, each requiring consideration of possible algorithms and data structures that would suit the task.
	The implementation needed to be able to:
	\begin{itemize}
		\item{represent the game board, the players and their positions}
		\item{record play history in order to show a replay of the game when finished}
		\item{undo and redo moves}
		\item{be able to run:}
		\begin{itemize}
			\item{human v human games}
			\item{human v computer games}
			\item{computer v computer games}
		\end{itemize}
	\end{itemize}
\section{Design}
\subsection{Language}
	The first consideration was which programming language to choose to develop the game. Some research on good languages for implementing Artificial Intelligence shows that most consider Python to be best suited. Qubit labs recommend it as it has a `simple and seamless structure' which makes it easy to test algorithms. They also site it's short development time and access to AI libraires \cite{Qubit}. One of the reasons the authors of \textit{Artificial Intelligence: A Modern Approach} decided to provide their algorithms in Python form was because of how close the implementation was to their pseudo-code \cite{Norvig}. With these recommendations I decided to use Python for it's simplicity and efficiency in order to concentrate on the way the algorithms work.
\subsection{Representing the game}
	The next consideration was how to represent the different aspects of the game. 
	A standard game of checkers requires: 
	\begin{itemize}
		\item{an 8x8 game board (with alternating black and white squares)}
		\item{two players}
		\item{24 pieces at the start which are characterised by:}
		\begin{itemize}
			\item{their colour (representing which player they belong to)}
			\item{their position on the board}
			\item{their type (man or king)}
		\end{itemize}
	\end{itemize}
	Initially I considered using a dictionary to represent the game board. Since a game board can be thought of as a grid of cells \{A1,..,A8,..,H1,..,H8\} these could be used as keys in the dictionary with a value of either \{` ', `X', `O', `KX', `KO'\} where ` ` represents an empty square, `X' a black piece, `O' a white piece and `K' a kinged piece. However, this seems like a waste of memory since the maximum number of cells which can have pieces in them is 24 at the start. This would leave 40 keys with blank values. Also, in checkers the pieces can only move onto the black cells meaning half of the dictionary would be empty for the whole game.
	
	Instead, I decided to use two separate dictionaries which only store each player's positions and the pieces in them. This means the game board itself need not be physically represented in memory since a function can be used to print out an image of the board onto the console with the player's pieces in the appropriate positions. I chose the dictionary structure because it is intuitive to think of a piece having a cell position on the board and this piece can be easily accessed using it's key. When it comes to checking if a player can move to a cell, a simple search of the two dictionaries will show if the cell is occupied. 
	
	After performing some tests on the performance of a linear search, binary search and the inbuilt Python search function for dictionaries (`in') I found that the `in' function was fastest by an order of magnitude and therefore, decided to use it as the search method (see Appendix 6.1).
	
	Using just these two dictionaries I am able to represent the board (abstractly) and both player's pieces and positions.
	
\subsection{Play history}
	In order to be able to watch a replay of a finished game, a complete play history must be recorded. A queue structure is ideal for this purpose. When a move is made, it is added to the end of the queue. When the replay function is activated the position dictionaries are reset and this move queue is traversed in FIFO (First In First Out) order, recreating the game.
	
\subsection{Undo/Redo}
	If a human player wishes to undo a move then they would need to reverse both their opponent's last move and their own last move. Only reversing the opponent's last move would most likely result in the opponent performing that same move again.
	
	We already have a record of both moves in the play history, however, if it is to be treated as a queue then we do not have easy access to them (since they are at the back of the queue). If the play history is instead implemented as a deque we can still keep the advantages of replaying in FIFO order and be able to access the end of the deque to get the last move. The information stored in each move can then be used to reverse it's effects on the position dictionaries.
	
	To redo a move, the moves which have just been undone must be reapplied. When the user chooses to undo a move, a record of this must be kept incase they wish to redo it. If the user undoes several moves and wishes to reapply them, then they need to be stored in the correct order. A stack would be an ideal data structure to store all undone moves. As can be seen from figure 1, the moves will stay in the correct order and the user can undo/redo as many moves as they like.
\begin{figure}[h]
        \centering
        \includegraphics[width=\columnwidth]{undo_redo}
        \caption{Undo/Redo function - applied by player 1}
    \end{figure}
    
\subsection{AI}
	A good AI player must be able to choose the move which would most likely result in a win state for the AI. I chose to implement my AI as the minimax algorithm with alpha-beta pruning and a cutoff depth (as provided in \textit{Artificial Intelligence: A Modern Approach} \cite{Russell}). A small example game tree is shown in Figure 2. 
	
	The leafs in this tree represent end states, MAX wants to choose the action which will maximise the worst case outcome. Assuming that MIN always plays optimally it will want to minimise the worst case outcome for MAX. MAX first considers action $a_1$ and discovers that the best outcome from this will be a score of 3 since MIN will choose the move which minimises this score. MAX then moves on to actions $a_2$ and $a_3$ but discovers that it will result in MIN choosing an action which results in an even smaller score. MAX therefore chooses action $a_1$. 
\begin{figure}[h]
        \centering
        \includegraphics[width=\columnwidth]{minimax}
        \caption{Minimax Game Tree \cite{Russell}}
    \end{figure}
    
	If the minimax algorithm traverses such a game tree then it needs to make a decision based on the scores of the state. This meant I had to choose a way to score the value of each game state. I decided to have different scoring methods depending on how far the game had progressed. At the start of the game the score is based on both the current player and the opponent's pieces.  Each `man' is worth 1 point and each `king' is worth 5. Conversely, an opponent's `man' is worth -1 point and an opponent's `king' is worth -5 points. This means that at the start the AI will actively seek out those boards where it has more kings than the opponent, giving it a better chance to win. In order to avoid loops or stalemates I decided that when an AI's opponent has less than five pieces left it should stop considering it's own pieces and only consider the opponent's. This avoids the AI hoarding it's pieces and instead opens up the possibility of sacrificing pieces in order to win.
	
	A downfall to minimax is that the number of game states is exponential to the number of moves. In checkers there can be many moves already available at the top of the game tree. A solution to this is to `prune' the tree so as to avoid these visits to redundant nodes. Alpha-beta pruning is one such method. Consider a node `n' in the tree, if the AI has a better move in the parent node of `n' (or at any choice point further up) then `n' will never be reached during actual play. Therefore, we can safely ignore `n' (it and the sub-tree below it, is pruned).
	
	Even with alpha-beta pruning, the algorithm can still take quite some time to choose a move, especially at the start of the game. When the AI is playing against a human it may get frustrating for the human having to wait for the AI to make a move. Introducing a cut-off depth to the AI's path through the tree solves this problem. It forces the algorithm to stop and instead of returning the score for the end state it returns the `utility' (how good a state is) for the depth it has reached and makes the decision based on that. This does mean that the AI may be easier to beat as it cannot see the full possibile outcomes of it's decision. I have introduced an option for the human player to change this cut-off depth to make it harder or easier to beat the computer. The easiest setting is depth 4 and the hardest, depth 10 (anymore than this results in a very long wait time between turns).
	
	If the user chooses to pit two AIs against each other they can also choose the difficulty setting of each.
	
\section{Enhancements}
	Possible improvements to be added to the game could include the following:
\begin{itemize}
	\item{A graphical user interface to make the board more user friendly}
	\item{Having the board highlight the cells where a piece can move to}
	\item{Being able to select the piece you wish to move instead of being presented with all the options}
	\item{Player's choice of colour}
	\item{A taken piece `bank' for each player}
	\item{Good move ordering in the alpha-beta pruning to reduce it's time complexity}
	\item{Storing repeated states in memory to avoid unnecessary visits}
\end{itemize}

\section{Critical Evaluation}
	\subsection{Replay}
	The replay function works well as the game is recorded by storing moves in the play history list. Each move has information on the player, the start and end cell of the move, the cell and type (`man' or `king') of a taken piece (if applicable) and whether or not this piece was kinged as a result of the move. When the board is reset to the starting positions, this list of moves is all that is needed to replay the game. The algorithm (shown in Algorithm 1) simply iterates through the moves, and updates the positions and pieces as necessary. 
	
\begin{algorithm}[h]
\ForEach{$move \in play\_history$}{
// Details of the move

 print\_move($move$)\;

// Perform the move

 update\_positions($positions$, $move$)\;

// King piece if necessary

\If{$move$ resulted in piece being 'kinged'}{
   king($player\_positions[move.end]$)\;
}

// Display the board

print($board$)\;

// Wait for user input to step through

wait()\;
}
\caption{Replay}
\end{algorithm}

	\subsection{Undo/Redo}
	The undo/redo function works well as the player is only presented with the options when appropriate (ie. can only undo when a move has actually been made and can only redo if an undo has occurred). When an undo is chosen by a player I decided that it makes sense to undo the last move made by the opponent and the move made by the player. Undoing only the opponents move would almost certainly result in the opponent simply repeating the move again (definitely the case for the AI). Therefore, when the player undo's, both moves are added to the redo stack and play returns to the player who chose to undo. Now the player is presented with the choice to make a different (or the same) move and also the option to undo again or redo. Undoing again, simply repeats the process until there are no longer any moves left to undo and redoing will pop the top two redo list entries back onto the play history list and re-perform those moves. Once a player commits to a move the redo list is cleared as it will no longer be valid.
	\subsection{Chain jumps}
	The rules of checkers dictate that when a jump move is available it must be taken. This is implemented when checking the legal moves at the start of the turn. The first check is whether or not there are jump moves available and, if so, standard moves are not checked. This populates the legal moves list with jump moves only, restricting the user to just these moves. Further, immediately after a jump move is performed (ie. before play is passed to the other player), if the new game state reveals another jump move available then the player must make this move also and so on, resulting in a `chain' of jump moves. I included a flag which indicates when a jump move occured. At the end of a move the game checks if this flag is set and, if so, it checks for further possible jump moves. If any are possible it keeps the player flag set to the current player. This means when this loop ends, play is kept by the current player and they then get to choose which jump move to make. This method keeps things simpler than storing a different kind of move in the play history. Instead, each link in the chain is just considered a standalone move. 
	
	Currently when the program checks for further possible jump moves it repopulates the legal moves list and if there are any, keeps play with this player. This is quite inefficient as the legal moves list is cleared and recalculated at the start of each turn and so the same check will have been performed twice. An improvement could be either to create a simpler check for further jump moves which does not recalculate the legal moves or have a check at the start of a turn to see whether or not this move is part of a chain and therefore the moves are already calculated.
	\subsection{AI}
	The AI works well with room for improvement. It is able to make a logical choice of move based upon the state of the board either at an end state or at a cutoff depth. It's difficulty can be increased by increasing this cutoff depth however this will make the time taken to choose a move exponentially longer. The score function currently only considers the types and number of pieces on the board. There are other considerations which may contribute to the `value' of a state. For example, a more effective score function could include the positions of the pieces as those closer to their opponents edge could be more valuable since they are more likely to become `kings'.
	
	Another possible improvement could be to implement forward pruning. This means that some moves are pruned without further consideration (decision based on some evaluation function). This could speed up the AI or allow it to probe further into the tree in the same time. The downfall here is that it may prune sub-trees which contain the best move.
	
\section{Personal Evaluation}
	During the course of this project I realised the benefit of choosing the most appropriate data structure for the behaviour desired. Initially I chose to use a dictionary to represent my game board and lists for my players piece positions however I realised how wasteful this was when I had to continually update all the separate structures. I instead opted to simply amalgamate them into two dictionaries storing each player's pieces.
	
	Whenever I was faced with a challenge in implementing the rules I found that going back to real-world checkers was the best way to solve it. Going through a game step by step and analysing what was happening at each stage broke down the problem and resulted in pseudo-code I could use to implement in my program.
	
	My greatest challenge was implementing the AI. I consulted the text \textit{Artificial Intelligence: A Modern Approach} and found AI algorithms specific to games. The minimax algorithm with alpha-beta pruning and a cutoff depth seemed like it was best suited to checkers since the game tree can be incredibly large. I had to decide on an evaluation function which would return the value of a state. After some research I found a variety of ways to do this and opted for a dynamic scoring function which considers both players pieces at the start and only the opponent's pieces towards the end of the game.
	
	I feel I performed well throughout this coursework. I successfully implemented all features required in the specification and made special considerations as to which algorithms and data structures would be best to use. 
	
	I would have preferred to have implemented a graphical user interface however I focussed on the quality of the program itself rather than get bogged down in the aesthetics.

\pagebreak

\section{Appendix}
\subsection{Search tests results}
\textit{Test code supplied in submission}

Linear search:  2.6684871409088375e-08

Binary search:  2.600274747237563e-08

In search:  7.1478425525128845e-09

Results in seconds
\bibliography{references}
		

\begin{thebibliography}{1}

\bibitem{Norvig} Stuart Russell, Peter Norvig {\em AIMA Python Code Guidelines}-  http://aima.cs.berkeley.edu/python/guidelines.html 2003.

\bibitem{Russell} Stuart Russell, Peter Norvig {\em Artificial Intelligence: A Modern Approach} 2010.

\bibitem{Qubit} Qubit Labs {\em 7 AI Programming Languages To Choose From} 2017.

\end{thebibliography}

\end{document}

