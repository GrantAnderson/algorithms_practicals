def generateRandomList(data,entries):
    import random
    for i in range(entries):
        data.append(random.randint(0, 1000))
    
def binarySearch(data,key):
    first = 0
    last = len(data)-1
    found = False

    while first <= last and not found:
        midpoint = (first + last)//2
        if data[midpoint] == key:
            found = True
            return midpoint
        else:
            if key < data[midpoint]:
                last = midpoint-1
            else:
                first = midpoint+1
    return -1

def main():
    import time
    
    data = list()
    key = list()
    generateRandomList(key, 250)
    
    table = list()
    total_times = 0.0

    for size in range(100,30100,100):
        generateRandomList(data,size)
        data.sort()
        for k in key:
            start_time = time.time()
            result = binarySearch(data,k)
            end_time = time.time()
        
            time_taken = end_time - start_time
            total_times = total_times + time_taken
            #if result == -1:
                #print("Key not found in array")
            #else:
                #print("Key ",k," found at index: ",result)

        average = total_times / len(key)
        table.append(average)
    for i in table:
        print(i)

if __name__ == "__main__":
    main()
