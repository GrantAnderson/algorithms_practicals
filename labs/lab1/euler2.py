acc = 0
i = 2
fib = [1, 2]

while fib[-1] < 4000000:
    if fib[-1] % 2 == 0:
        acc = acc + fib[-1]
    fib.append(fib[i-1]+fib[i-2])
    i = i + 1
print(acc)
