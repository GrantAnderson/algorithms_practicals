import string
import time

def linearSearch(data,key):
    for i, v in enumerate(data):
        if v == key:
            return i
    return -1

def main():

    # define punctuation
    punctuations = set(string.punctuation)

    # Hash table to store words
    words = dict()
    # Open text file
    file = open("lots.txt", "r")

    # Record the start time
    start_time = time.time()

    # For each line in the file
    for line in file:
        # Split the line into words
        line_split = line.split()
        # For each word in the line
        for word in line_split:
            # Remove punctuation
            word = ''.join(ch for ch in word if ch not in punctuations)
            # Convert to lower case
            word = word.lower()
            # Check words list to see if this word has been seen already
            result = linearSearch(words, word)
            # If word has been seen, increment the count
            if result != -1:
                words[word] = words[word] + 1
            # If word has not been seen, create a new one and add to the list
            else:
                words[word] = 1

    # Calculate total time taken and display
    total_time = time.time() - start_time
    print(total_time)
    #Display words
    #for word in words:
        #print(word, words[word])
    
if __name__ == "__main__":
    main()
