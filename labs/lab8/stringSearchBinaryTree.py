import string
import time

class Node:   
    def __init__(self, word, count, left, right):
        self.word = word
        self.count = count
        self.left = left
        self.right = right

def preOrder(root):
    if root == None:
        return
    print(root.word, root.count)
    preOrder(root.left)
    preOrder(root.right)

def contains(root, target):
    if root == None:
        return False
    else:
        if target == root.word:
            root.count = root.count + 1
            return True
        else:
            if target < root.word:
                return contains(root.left, target)
            else:
                return contains(root.right, target)

def insert(root, data):
    if root == None:
        root = Node(data, 1, None, None)
    else:
        if data <= root.word:
            root.left = insert(root.left, data)
        else:
            root.right = insert(root.right, data)
    return root

def main():

    # define punctuation
    punctuations = set(string.punctuation)

    # Hash table to store words
    root = None
    # Open text file
    file = open("lots.txt", "r")

    # Record the start time
    start_time = time.time()

    # For each line in the file
    for line in file:
        # Split the line into words
        line_split = line.split()
        # For each word in the line
        for word in line_split:
            # Remove punctuation
            word = ''.join(ch for ch in word if ch not in punctuations)
            # Convert to lower case
            word = word.lower()
            # Check words list to see if this word has been seen already and
            # if so, increment the count
            result = contains(root, word)
            # If word has not been seen, create a new one and add to the list
            if result == False:
                root = insert(root, word)

    # Calculate total time taken and display
    total_time = time.time() - start_time
    print(total_time)
    #Display words
    #preOrder(root)
    
if __name__ == "__main__":
    main()
