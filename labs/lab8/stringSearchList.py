import string
import time

class Word:
    def __init__(self, name, count):
        self.name = name
        self.count = count

def main():

    # define punctuation
    punctuations = set(string.punctuation)

    
    # Dynamically sized list to store words
    words = list()
    # Open text file
    file = open("lots.txt", "r")

    # Record the start time
    start_time = time.time()

    # For each line in the file
    for line in file:
        # Split the line into words
        line_split = line.split()
        # For each word in the line
        for word in line_split:
            # Remove punctuation
            word = ''.join(ch for ch in word if ch not in punctuations)
            # Convert to lower case
            word = word.lower()
            # Check words list to see if this word has been seen already
            flag = False
            for w in words:
                # If word has been seen, increment the count
                if w.name == word:
                    w.count = w.count + 1
                    flag = True
            # If word has not been seen, create a new one and add to the list
            if flag == False:
                w = Word(word, 1)
                words.append(w)

    # Calculate total time taken and display
    total_time = time.time() - start_time
    print(total_time)
    #Display words
    #for word in words:
        #print(word.name, word.count)
    
if __name__ == "__main__":
    main()
