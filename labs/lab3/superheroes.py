def main():
    import time
    heroSet = set(['Iron Man','Batman','Superman','Captain America','Hulk','Thor'])

    heroList = list()
    heroList.append('Iron Man')
    heroList.append('Batman')
    heroList.append('Superman')
    heroList.append('Captain America')
    heroList.append('Hulk')
    heroList.append('Thor')

    print("List search:")
    start_time = time.time()
    for i in range(0,2000000):
        'Hulk' in heroList
    end_time = time.time()
    print(end_time - start_time)

    print("Set search: ")
    start_time = time.time()
    for i in range(0,2000000):
        'Hulk' in heroSet
    end_time = time.time()
    print(end_time - start_time)

    print("List add:")
    start_time = time.time()
    for i in range(0,2000000):
        hero = 'New hero'
        hero += str(i)
        heroList.append(hero)
    end_time = time.time()
    print(end_time - start_time)

    print("Set add: ")
    start_time = time.time()
    for i in range(0,2000000):
        hero = 'New hero'
        hero += str(i)
        heroSet.add(hero)
    end_time = time.time()
    print(end_time - start_time)

if __name__ == "__main__":
    main()
