def main():
    tel = {'grant': 2041, 'douglas':3020}
    tel['donna'] = 4127
    print(tel)
    del tel['grant']
    print(tel)
    print(tel['donna'])
    
if __name__ == "__main__":
    main()
