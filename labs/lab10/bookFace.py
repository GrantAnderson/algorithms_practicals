


def main():
    # Adjaceny matrix
    adjMatrix = list()
    # Number of people
    n = 6
    # Initialise the matrix
    w = 6
    h = 6
    adjMatrix = [[0 for x in range(w)] for y in range(h)] 
    for i in range(0, n):
        for j in range(0, n):
            adjMatrix[i][j] = float('inf')
        adjMatrix[i][i] = 0;


if __name__ == "__main__":
    main()
