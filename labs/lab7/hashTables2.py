def h1(k):
    return k % 1000

def h(k, i):
    return (h1(k) + i) % 1000

def insertHash(hashList, key, collisions, insertions, probes):
    # Record number of probes in this insertion
    number_probes = 0
    for i in range(len(hashList) - 1):
        # Check if index is free
        if hashList[h(key, i)] == " ":
            # Remove blank
            hashList.pop(h(key, i))
            #Insert new value
            hashList.insert(h(key,i), key)
            # Increment insertions
            insertions[0] += 1
            # Save number of probes for this insertion
            probes.append(number_probes)
            return
        else:
            # Increment number of probes
            number_probes += 1
            # Increment number of collisions
            collisions[0] += 1

def generateRandomList(data,entries):
    import random
    for i in range(entries):
        data.append(random.randint(1000, 9999))

def main():
    # Create an empty list
    hashList = list()
    for i in range(0,1000):
        hashList.append(" ")

    # Generate random 4 digit numbers
    numbers = list()
    size = 1000
    generateRandomList(numbers, size)

    # Records for statistics
    collisions = list()
    collisions.append(0)
    insertions = list()
    insertions.append(0)
    probes = list()

    # Insert the numbers into the hash list
    for i in numbers:
        insertHash(hashList, i, collisions, insertions, probes)

    # Calculate mean number of probes for insertions
    total_probes = list()
    total_probes.append(0)
    for i in range(0, len(probes) - 1):
        total_probes[0] += probes[i] 
    mean_probes = total_probes[0] / len(probes)
    
    # Print results
    print(hashList)
    print("Collisions: ", collisions[0])
    print("Insertions: ", insertions[0])
    print("Frequency of collisions: ", collisions[0] / insertions[0])
    print("Mean of probes: ", mean_probes)

if __name__ == "__main__":
    main()
