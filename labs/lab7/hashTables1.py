def h1(k):
    return k % 11

def h(k, i):
    return (h1(k) + i) % 11

def insertHash(hashList, key):
    for i in range(len(hashList) - 1):
        if hashList[h(key, i)] == " ":
            # Remove blank
            hashList.pop(h(key, i))
            #Insert new value
            hashList.insert(h(key,i), key)
            return

def main():
    numbers = [6, 2, 13, 5, 17, 39, 1, 12]
    hashList = list()
    for i in range(0,11):
        hashList.append(" ")

    for n in numbers:
        insertHash(hashList, n)

    print(hashList)
        
if __name__ == "__main__":
    main()
