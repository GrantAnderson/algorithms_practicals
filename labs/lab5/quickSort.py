def quickSort(data, low_idx, high_idx):
    # Implement quick sort algorithm
    i = low_idx
    j = high_idx
    # Get the pivot element from the middle of the list
    pivot = data[low_idx + int((high_idx - low_idx) / 2)]
    # Partition the list
    while i <= j:
        while data[i] < pivot:
            i = i + 1
        while data[j] > pivot:
            j = j - 1
        if i <= j:
            tmp = data[i]
            data[i] = data[j]
            data[j] = tmp
            i = i + 1
            j = j - 1
    # Recursion
    if low_idx < j:
        quickSort(data, low_idx, j)
    if i < high_idx:
        quickSort(data, i, high_idx)
        
def generateRandomList(data,entries):
    import random
    for i in range(entries):
        data.append(random.randint(0, 1000))
    
def main():
    import time
    
    table = dict()
    total_time = 0
    for i in range(100, 20001, 100):
        for j in range(1,10):
            data = list()
            generateRandomList(data,i)
            
            length = len(data)
            #print("Unsorted list", data)
            start_time = time.time()
            quickSort(data, 0, length - 1)
            end_time = time.time()
            #print("Sorted list", data)

            time_taken = end_time - start_time
            total_time = total_time + time_taken
            
        average = total_time / i
        table[i] = average
    for i in table:
        print(table[i])


    
if __name__ == "__main__":
    main()
