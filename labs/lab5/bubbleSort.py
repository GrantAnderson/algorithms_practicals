def bubbleSort(data):
    # Implement bubble sort algorithm
    
    # set flag to true to begin first pass
    flag = True
    while flag:
        # set flag to false awaiting a possible swap
        flag = False
        for i in range(0, len(data) - 1):
            # ascending order
            if data[i] > data[i + 1]:
                # swap elements
                temp = data[i]
                data[i] = data[i+1]
                data[i + 1] = temp
                # shows a swap occurred
                flag = True
        
def generateRandomList(data,entries):
    import random
    for i in range(entries):
        data.append(random.randint(0, 1000))
    
def main():
    import time
    
    table = dict()
    total_time = 0
    for i in range(100, 10001, 100):
        for j in range(1,10):
            data = list()
            generateRandomList(data,i)
            
            #print("Unsorted list", data)
            start_time = time.time()
            bubbleSort(data)
            end_time = time.time()
            #print("Sorted list", data)

            time_taken = end_time - start_time
            total_time = total_time + time_taken
            
        average = total_time / i
        table[i] = average
    for i in table:
        print(table[i])

if __name__ == "__main__":
    main()
