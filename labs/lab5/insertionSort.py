def insertionSort(data):
    # Implement insertion sort algorithm
    size = len(data)
    for j in range(1, size):
        key = data[j]
        i = j - 1
        while (i > -1) and (data[i] > key):
            data[i + 1] = data[i]
            i = i - 1
        data[i + 1] = key
        
def generateRandomList(data,entries):
    import random
    for i in range(entries):
        data.append(random.randint(0, 1000))
    
def main():
    import time
    
    table = dict()
    total_time = 0
    for i in range(100, 10001, 100):
        for j in range(1,10):
            data = list()
            generateRandomList(data,i)
            
            #print("Unsorted list", data)
            start_time = time.time()
            insertionSort(data)
            end_time = time.time()
            #print("Sorted list", data)

            time_taken = end_time - start_time
            total_time = total_time + time_taken
            
        average = total_time / i
        table[i] = average
    for i in table:
        print(table[i])


if __name__ == "__main__":
    main()
