def selectionSort(data):
    # Implement selection sort algorithm
    for outer in range(0, len(data) - 1):
        min_idx = outer
        for inner in range(outer, len(data)):
            if data[inner] < data[min_idx]:
                min_idx = inner
        tmp = data[outer]
        data[outer] = data[min_idx]
        data[min_idx] = tmp
        
def generateRandomList(data,entries):
    import random
    for i in range(entries):
        data.append(random.randint(0, 1000))
    
def main():
    import time
    
    table = dict()
    total_time = 0
    for i in range(100, 10001, 100):
        for j in range(1,10):
            data = list()
            generateRandomList(data,i)
            
            #print("Unsorted list", data)
            start_time = time.time()
            selectionSort(data)
            end_time = time.time()
            #print("Sorted list", data)

            time_taken = end_time - start_time
            total_time = total_time + time_taken
            
        average = total_time / i
        table[i] = average
    for i in table:
        print(table[i])

if __name__ == "__main__":
    main()
