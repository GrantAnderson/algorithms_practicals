def evaluate(op1, op2, operator):
    if operator == '*':
        return op2 * op1
    if operator == '/':
        return op2 / op1
    if operator == '-':
        return op2 - op1
    if operator == '+':
        return op2 + op1
    return 0;

def evaluatePostfix(postfix):
    stack = list()
    for i in range(0, len(postfix)):
        char = postfix[i]
        if char == " ":
            continue
        elif char not in {'*','/','-','+'}:
            stack.append(char)
        else:
            op1 = int(stack.pop())
            op2 = int(stack.pop())
            result = evaluate(op1, op2, char)
            stack.append(result)
    return result

def main():
    postfix = input("Please enter postfix expression: ")
    result = evaluatePostfix(postfix)
    print(result)

if __name__ == "__main__":
    main()
