class Node:   
    def __init__(self, number, left, right):
        self.number = number
        self.left = left
        self.right = right

class BinaryTree:
    def __init__(self, root):
        # Root of the tree
        self.root = root
    
    def preOrder(self, root):
        if root == None:
            return
        print(root.number)
        self.preOrder(root.left)
        self.preOrder(root.right)

    def contains(self, target):
        if self.root == None:
            return False
        else:
            if target == self.root.number:
                return True
            else:
                if target < self.root.number:
                    return contains(self.root.left, target)
                else:
                    return contains(self.root.right, target)

    def insert(self, root, data):
        if root == None:
            root = Node(data, None, None)
        else:
            if data <= root.number:
                root.left = self.insert(root.left, data)
            else:
                root.right = self.insert(root.right, data)
        return root

def main():
    root = None
    tree = BinaryTree(root)
    numbers = list()
    numbers.append(2)
    numbers.append(3)
    numbers.append(5)
    numbers.append(1)
    
    for i in numbers:
        root = tree.insert(root, i)

    tree.preOrder(root)
    
if __name__ == "__main__":
    main()
