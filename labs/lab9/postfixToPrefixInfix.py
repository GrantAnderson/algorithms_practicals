def postfixToInfix(postfix):
    stack = list()
    for i in range(0, len(postfix)):
        char = postfix[i]
        if char == " ":
            continue
        elif char not in {'*','/','-','+'}:
            stack.append(char)
        else:
            op1 = stack.pop()
            op2 = stack.pop()
            op = "(" + op2 + char + op1 + ")"
            stack.append(op)
    return stack


def postfixToPrefix(postfix):
    stack = list()
    for i in range(0, len(postfix)):
        char = postfix[i]
        if char == " ":
            continue
        elif char not in {'*','/','-','+'}:
            stack.append(char)
        else:
            op1 = stack.pop()
            op2 = stack.pop()
            op = char + op2 + op1
            stack.append(op)
    return stack

def main():
    postfix = input("Please enter postfix expression: ")
    prefix = postfixToPrefix(postfix)
    infix = postfixToInfix(postfix)
    print(prefix)
    print(infix)

if __name__ == "__main__":
    main()
