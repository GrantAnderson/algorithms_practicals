def print_board(rowA, rowB, rowC):
    for i, square in enumerate(rowA):
        if i < 2:
            print(square, "| ", end='')
        else:
            print(square)
    
    for i, square in enumerate(rowB):
        if i < 2:
            print(square, "| ", end='')
        else:
            print(square)

    for i, square in enumerate(rowC):
        if i < 2:
            print(square, "| ", end='')
        else:
            print(square)

def check_square(move,player,rowA,rowB,rowC):
    mark = ""
    if player == 1:
        mark = "X"
    else:
        mark = "O"
    if move[0] == "A":
        if move[1] == "1":
            if rowA[0] != "-":
                print("Error")
                return 1
            else:
                rowA.pop(0)
                rowA.insert(0, mark)
        elif move[1] == "2":
            if rowA[1] != "-":
                print("Error")
                return 1
            else:
                rowA.pop(1)
                rowA.insert(1, mark)
        elif move[1] == "3":
            if rowA[2] != "-":
                print("Error")
                return 1
            else:
                rowA.pop(2)
                rowA.insert(2, mark)
                
    if move[0] == "B":
        if move[1] == "1":
            if rowB[0] != "-":
                print("Error")
                return 1
            else:
                rowB.pop(0)
                rowB.insert(0, mark)
        elif move[1] == "2":
            if rowB[1] != "-":
                print("Error")
                return 1
            else:
                rowB.pop(1)
                rowB.insert(1, mark)
        elif move[1] == "3":
            if rowB[2] != "-":
                print("Error")
                return 1
            else:
                rowB.pop(2)
                rowB.insert(2, mark)

    if move[0] == "C":
        if move[1] == "1":
            if rowC[0] != "-":
                print("Error")
                return 1
            else:
                rowC.pop(0)
                rowC.insert(0, mark)
        elif move[1] == "2":
            if rowC[1] != "-":
                print("Error")
                return 1
            else:
                rowC.pop(1)
                rowC.insert(1, mark)
        elif move[1] == "3":
            if rowC[2] != "-":
                print("Error")
                return 1
            else:
                rowC.pop(2)
                rowC.insert(2, mark)

def checkEqual3(lst):
   return lst[1:] == lst[:-1]

def clear_board(rowA,rowB,rowC):
    for i in range(3):
        rowA.pop(i)
        rowB.pop(i)
        rowC.pop(i)
        rowA.insert(i,"-")
        rowB.insert(i,"-")
        rowC.insert(i,"-")

def replay(moveList,rowA,rowB,rowC):
    for i in range(len(moveList)):
        move = moveList.popleft()
        if len(moveList) % 2 == 0:
            check_square(move,1,rowA,rowB,rowC)
            print("Player 1: ")
        else:
            check_square(move,2,rowA,rowB,rowC)
            print("Player 2: ")
        print_board(rowA,rowB,rowC)

def main():
    from collections import deque
    rowA = list()
    rowB = list()
    rowC = list()

    moveList = deque()

    for i in range(3):
        rowA.append("-")
        rowB.append("-")
        rowC.append("-")

    print_board(rowA, rowB, rowC)

    while True:
        if len(moveList) % 2 == 0:
            move = input("P1 enter your move: ")
            player = 1
            if (check_square(move,player,rowA,rowB,rowC) == 1):
                continue
            moveList.append(move)
            print_board(rowA,rowB,rowC)
            if checkEqual3(rowA) and rowA[0] == "X":
                print("Player 1 wins")
                break
            if checkEqual3(rowB) and rowB[0] == "X":
                print("Player 1 wins")
                break
            if checkEqual3(rowC) and rowC[0] == "X":
                print("Player 1 wins")
                break
        else:
            move = input("P2 enter your move: ")
            player = 2
            if (check_square(move,player,rowA,rowB,rowC) == 1):
                continue
            moveList.append(move)
            print_board(rowA,rowB,rowC)
            if checkEqual3(rowA) and rowA[0] == "O":
                print("Player 2 wins")
                break
            if checkEqual3(rowB) and rowB[0] == "O":
                print("Player 2 wins")
                break
            if checkEqual3(rowC) and rowC[0] == "O":
                print("Player 2 wins")
                break
        if "-" not in rowA and "-" not in rowB and "-" not in rowC:
            break
    print("Reset")
    clear_board(rowA,rowB,rowC)
    print_board(rowA,rowB,rowC)
    replay(moveList,rowA,rowB,rowC)
    
if __name__ == "__main__":
    main()
